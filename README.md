Menu Manager System for Spigot Plugins

This is meant to serve as a template for complex spigot plugins with complex GUI systems. This cuts down on the clutter and creates an easily manageable layout.

Each menu can be extended by the Menu or PaginatedMenu class which provides the basic menu functionality and customizable options for each menu.

Also, there is the PlayerMenuUtility which serves as companion object to each menu which provides all the information like the player who is using the menu, and any other data you would like to pass along all the various inventories that make up the inventory system.



